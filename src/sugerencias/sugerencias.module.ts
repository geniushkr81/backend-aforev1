import { Module } from '@nestjs/common';
import { SugerenciasService } from './sugerencias.service';
import { SugerenciasController } from './sugerencias.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SugerenciaSchema } from './schemas/sugerencia.schema';

@Module({
  imports:[MongooseModule.forFeature([{
    name:'Sugerencia',
    schema:SugerenciaSchema
  }])],
  providers: [SugerenciasService],
  controllers: [SugerenciasController]
})
export class SugerenciasModule {}
