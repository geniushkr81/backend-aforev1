import { Document } from 'mongoose';
export interface Sugerencia extends Document{
    readonly name: string;
    readonly quality:string;
    readonly product:string;
    readonly price:string;
    readonly createdAt: Date;
}