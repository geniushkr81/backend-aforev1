import { Test, TestingModule } from '@nestjs/testing';
import { SugerenciasController } from './sugerencias.controller';

describe('Sugerencias Controller', () => {
  let controller: SugerenciasController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SugerenciasController],
    }).compile();

    controller = module.get<SugerenciasController>(SugerenciasController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
