import { Body, Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';
import { SugerenciasService } from './sugerencias.service';
import { SugerenciaDTO } from './dto/sugerencia.dto';

@Controller('sugerencias')
export class SugerenciasController {
    constructor(private sugerencia:SugerenciasService){}
        
    // categories create a new record
    @Post('/create')
    async addCategory(@Res() res, @Body() SugerenciaDTO: SugerenciaDTO){
        const categories = await this.sugerencia.create(SugerenciaDTO);
        return res.status(HttpStatus.OK).json({
            message: "Post category has been created",
            categories
        });
    }
  
    @Get('all')
    async GetAll(@Res() res){
        const categories = await this.sugerencia.getAll();
        console.log(categories);
        return res.status(HttpStatus.OK).json(categories);
    }
}
