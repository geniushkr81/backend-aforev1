import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Sugerencia } from './interfaces/sugerencia.interface';
import { SugerenciaDTO } from './dto/sugerencia.dto';
import { Model } from 'mongoose';

@Injectable()
export class SugerenciasService {
    constructor(@InjectModel('Sugerencia') private readonly SugerenciaModel: Model<Sugerencia>){}

    async create(SugerenciaDTO: SugerenciaDTO): Promise<any> {
        const createdDevice = new this.SugerenciaModel(SugerenciaDTO);
        return createdDevice.save();
    }

  
    async getAll(): Promise<any>{
        return await this.SugerenciaModel.find();
    }
}
