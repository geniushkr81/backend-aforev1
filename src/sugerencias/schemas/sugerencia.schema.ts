import * as mongoose from 'mongoose';

export const SugerenciaSchema = new mongoose.Schema({
    name: String,
    quality:String,
    product:String,
    price:String,
    createdAt: Date,
},
{
    timestamps:true
});