import { Schema } from 'mongoose'

export const CreditoSchema = new Schema({ 
     name: String,
     rfc: String,
     mail: String,
     phone: String,
     age: String,
     monto: String,
     tienecred: String,
     status: String
})