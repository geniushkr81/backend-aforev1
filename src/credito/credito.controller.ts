import { Controller, Post, Get, Put, Delete, Res, HttpStatus, Body, NotFoundException, Query, Param } from '@nestjs/common';
import {CreateCreditoDto} from './dto/credito.dto';
import {CreditoService} from './credito.service';

@Controller('credito')
export class CreditoController {

    constructor (private creditoService: CreditoService){}

    @Post('/create')
    async createCredito(@Res() res, @Body() createCreditoDto: CreateCreditoDto){
        const credito = await this.creditoService.createCredito(createCreditoDto);
        return res.status(HttpStatus.OK).json({
            message : 'Credito ha sido creado',
            credit: credito
        })
    }

    @Get('/all')
    async getCreditos(@Res() res){
        const creditos = await this.creditoService.getCreditos();
        return res.status(HttpStatus.OK).json({
            creditos
        })
    }

    @Get('/:rfc')
    async getCredito(@Res() res, @Param('rfc') rfc:string){
        const credito = await this.creditoService.getCredito(rfc);
        if(!credito) throw new NotFoundException('Credito no existe');
        return res.status(HttpStatus.OK).json(credito);
    } 

    @Put('/update')
    async updateCredito(@Res() res, @Body() createCreditoDto: CreateCreditoDto, @Query('creditoID') creditoId){
        const updatedCredito = await this.creditoService.updateCredito(creditoId, createCreditoDto);
        if(!updatedCredito) throw new NotFoundException('El Credito no existe, no podemos completar esta accion');
        return res.status(HttpStatus.OK).json({
            message: 'Credito actualizado',
            updatedCredito
        })
    }

    @Delete('/delete')
    async deleteCredito(@Res() res, @Query('creditoID') creditoID){
        const deletedCredito = await this.creditoService.deleteCredito(creditoID);
        if(!deletedCredito) throw new NotFoundException('El Credito no existe, no podemos completar esta acción');
        return res.status(HttpStatus.OK).json({
            message: 'Credito eliminado',
            deletedCredito
        })
    }

}
