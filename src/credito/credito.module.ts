import { Module } from '@nestjs/common';
import {CreditoController} from './credito.controller';
import {CreditoService} from './credito.service';
import {CreditoSchema} from './schemas/credito.schema';
import {MongooseModule} from '@nestjs/mongoose';

@Module({
    imports: [MongooseModule.forFeature([
        {name: 'Credito', schema: CreditoSchema}
        ])
    ],
    controllers: [CreditoController],
    providers: [CreditoService]
})
export class CreditoModule {}
