import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {Credito} from './interfaces/credito.interface';
import {CreateCreditoDto} from './dto/credito.dto';

@Injectable()
export class CreditoService {

    constructor(@InjectModel('Credito') private readonly creditoModel: Model<Credito>){}

    async getCreditos(): Promise<Credito[]>{
        const creditos = await this.creditoModel.find();
        return creditos;
    }

    async getCredito(rfc: string): Promise<Credito>{
        const credito = await this.creditoModel.findOne({rfc:rfc});
        return credito;
    }

    async createCredito(createCreditoDto: CreateCreditoDto): Promise<Credito>{
        const credito = new this.creditoModel(createCreditoDto);
        return await credito.save();
    }

    async updateCredito(creditoID: string, createCreditoDto: CreateCreditoDto): Promise<Credito>{
        const updatedCredito = await this.creditoModel.findByIdAndUpdate(creditoID, createCreditoDto, {new: true});
        return updatedCredito;
    }

    async deleteCredito(creditoID: string): Promise<Credito>{
        const deletedCredito = await this.creditoModel.findByIdAndDelete(creditoID);
        return deletedCredito;
    }
}
