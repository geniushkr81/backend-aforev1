import { Document } from "mongoose";

export interface Credito extends Document{
    
    readonly name: string;
    readonly rfc: string;
    readonly mail: string;
    readonly phone: string;
    readonly age: string;
    readonly monto: string;
    readonly tienecred: string;
    readonly status: string;
}