import { Body, Controller, Get, HttpStatus, Post, Res, Param, NotFoundException } from '@nestjs/common';
import { LoginService } from './login.service';
import { LoginDTO } from './dto/login.dto';

@Controller('login')
export class LoginController {
    constructor(private member:LoginService){}

    @Post('/create')
    async addLogin(@Res() res, @Body() LoginDTO: LoginDTO){
        const lists = await this.member.create(LoginDTO);
        return res.status(HttpStatus.OK).json({
            
            message: "Post has been created successfully",
            lists
        });
    }
    @Get('/:email')
    async GetMember(@Res() Res, @Param('email') info:string){
        const member = await this.member.validate(info);
        if(!member) throw new NotFoundException("Member not found !!!.");
        return Res.status(HttpStatus.OK).json(member);
    }

    @Get('all')
    async getUsuarios(@Res() res){
        const creditos = await this.member.getAll();
        console.log(creditos);
        return res.status(HttpStatus.OK).json(creditos);
    }

     @Get('/todos')
     async GetAll(@Res() res){
         const members = await this.member.getMembers();
         return res.status(HttpStatus.OK).json({
             members
         })
     }
}
