import { Document } from "mongoose";

export interface Captura extends Document{
    readonly name: string;
    readonly mail: string;
    readonly password: string;
    readonly curp: string;
    readonly rfc: string;
    readonly nss: string;
    readonly phone: string;
    readonly status: string;
}