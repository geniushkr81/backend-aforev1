import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'
import { Captura } from './interfaces/services.interface'
import { CreateCapturaDto } from './dto/services.dto'

@Injectable()
export class ServicesService {

    constructor(@InjectModel('Captura') private readonly capturaModel: Model<Captura>){}

    async getCapturas(): Promise<Captura[]>{
        const capturas = await this.capturaModel.find();
        return capturas;
    }

    async getCaptura(mail: string): Promise<Captura>{
        const captura = await this.capturaModel.findOne({mail:mail});
        return captura;
    }

    async createCaptura(createCapturaDTO: CreateCapturaDto): Promise<Captura>{
        const captura = new this.capturaModel(createCapturaDTO);
        return await captura.save();
     }
 
     async deleteCaptura(capturaID: string ): Promise<Captura>{
         const deletedCaptura = await this.capturaModel.findByIdAndDelete(capturaID);
         return deletedCaptura;
     }

     async updateCaptura(capturaID: string ,createCapturaDTO: CreateCapturaDto): Promise<Captura>{
        const updatedCaptura = await this.capturaModel.findByIdAndUpdate(capturaID, createCapturaDTO,{new: true});
        return updatedCaptura;
    }
}
