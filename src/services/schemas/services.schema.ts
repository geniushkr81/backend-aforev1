import { Schema } from 'mongoose'

export const CapturaSchema = new Schema({
    name: String,
    mail: String,
    password: String,
    curp: String,
    rfc: String,
    nss: String,
    phone: String,
    status: String
})