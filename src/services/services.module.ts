import { Module } from '@nestjs/common';
import { ServicesController } from './services.controller';
import { ServicesService } from './services.service';
import { MongooseModule } from '@nestjs/mongoose'
import {CapturaSchema} from './schemas/services.schema'

@Module({
  imports: [MongooseModule.forFeature([
    {name: 'Captura', schema: CapturaSchema}
    ])
  ],
  controllers: [ServicesController],
  providers: [ServicesService]
})
export class ServicesModule {}
