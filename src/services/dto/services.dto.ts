export class CreateCapturaDto {
    readonly name: string;
    readonly mail: string;
    readonly password: string;
    readonly curp: string;
    readonly rfc: string;
    readonly nss: string;
    readonly phone: string;
    readonly status: string;
}