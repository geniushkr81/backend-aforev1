import { Controller, Post, Get, Put, Delete, Res, HttpStatus, Body, NotFoundException, Query, Param} from '@nestjs/common';
import { CreateCapturaDto } from './dto/services.dto';
import { ServicesService } from './services.service';


@Controller('services')
export class ServicesController {

    constructor (private serviceService: ServicesService){}

    @Post('/create')
    async createCaptura(@Res() res, @Body() createCapturaDTO: CreateCapturaDto){
        const captura = await this.serviceService.createCaptura(createCapturaDTO);
        return res.status(HttpStatus.OK).json({
            message: "Captura creada",
            user: captura
        })
    }

    @Get('/all')
    async getCapturas(@Res() res){
        const capturas = await this.serviceService.getCapturas();
        return res.status(HttpStatus.OK).json({
            capturas
        })
    }

    @Get('/:mail')
    async getCaptura(@Res() res, @Param('mail') mail:string){
        const captura = await this.serviceService.getCaptura(mail);
        if(!captura) throw new NotFoundException('Captura de AFORE no existe');
        return res.status(HttpStatus.OK).json(captura);
    }

    @Delete('/delete')
    async deleteCaptura(@Res() res, @Query('capturaID') capturaID){
        const deletedCaptura = await this.serviceService.deleteCaptura(capturaID);
        if(!deletedCaptura) throw new NotFoundException('User does not exists, we cant complete the action');
        return res.status(HttpStatus.OK).json({
            message: 'User deleted',
            deletedCaptura
        });
    }

    @Put('/update')
    async updateCaptura(@Res() res, @Body() createCapturaDTO: CreateCapturaDto , @Query('capturaID') capturaID){
        const updtaedCaptura = await this.serviceService.updateCaptura(capturaID, createCapturaDTO);
        if(!updtaedCaptura) throw new NotFoundException('Cpatura does not exists, we cant complete the action');
        return res.status(HttpStatus.OK).json({
            message: 'Captura updated',
            updtaedCaptura
        });
    }
}
