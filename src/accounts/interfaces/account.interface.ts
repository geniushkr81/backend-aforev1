import { Document } from 'mongoose';
export interface Account extends Document{
    readonly code:string;
    readonly name:string;
    readonly curp:string;
    readonly nss:string;
    readonly email:string;
    readonly afore:string;
    readonly semanas:number;
    readonly monto:number;
    readonly estatus:number;
}