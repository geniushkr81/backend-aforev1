import * as mongoose from 'mongoose';

export const AccountSchema = new mongoose.Schema({
    code:String,
    name:String,
    curp:String,
    nss:String,
    email:String,
    afore:String,
    semanas:Number,
    monto:Number,
    estatus:Number
},{timestamps: true});