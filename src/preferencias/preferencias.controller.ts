import { Controller,Post,Body,Query, Param,Put, NotFoundException, 
    Get, Res, HttpStatus, UseGuards} from '@nestjs/common';
    import { PreferenciasService } from './preferencias.service';
    import { PreferenciaDTO } from './dto/preferencia.dto';
    import { JwtAuthGuard } from '../auth/jwt-auth.guard';
    import { AuthGuard } from '@nestjs/passport';
    
    
    @Controller('preferencias')
    
    
    export class PreferenciasController {
        constructor(private preferencia:PreferenciasService){}
        
        // categories create a new record
        @Post('/create')
        async addCategory(@Res() res, @Body() PreferenciaDTO: PreferenciaDTO){
            const categories = await this.preferencia.create(PreferenciaDTO);
            return res.status(HttpStatus.OK).json({
                message: "Post category has been created",
                categories
            });
        }
      
        @Get('all')
        async GetAll(@Res() res){
            const categories = await this.preferencia.getAll();
            console.log(categories);
            return res.status(HttpStatus.OK).json(categories);
        }
        // update info
        // update image
        
    }
    
    