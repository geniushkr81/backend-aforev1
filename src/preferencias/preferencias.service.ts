import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Preferencia } from './interfaces/preferencia.interface';
import { PreferenciaDTO } from './dto/preferencia.dto';


@Injectable()
export class PreferenciasService {
    constructor(@InjectModel('Preferencia') private readonly PreferenciaModel: Model<Preferencia>){}

    async create(PreferenciaDTO: PreferenciaDTO): Promise<any> {
        const createdDevice = new this.PreferenciaModel(PreferenciaDTO);
        return createdDevice.save();
    }

  
    async getAll(): Promise<any>{
        return await this.PreferenciaModel.find();
    }
  
}
