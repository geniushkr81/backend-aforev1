import * as mongoose from 'mongoose';

export const PreferenciaSchema = new mongoose.Schema({
    preferencia:String,
    createdAt: Date,
}, {timestamps:true})