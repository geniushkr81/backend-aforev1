import { Module } from '@nestjs/common';
import { PreferenciasService } from './preferencias.service';
import { PreferenciasController } from './preferencias.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PreferenciaSchema } from './schemas/preferencia.schema';

@Module({
  imports:[MongooseModule.forFeature([{
    name:'Preferencia',
    schema:PreferenciaSchema
  }])],
  providers: [PreferenciasService],
  controllers: [PreferenciasController]
})
export class PreferenciasModule {}
