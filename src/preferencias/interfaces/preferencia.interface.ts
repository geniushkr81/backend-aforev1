import { Document } from 'mongoose';
export interface Preferencia extends Document{
    readonly preferencia:string;
    readonly createdAt: Date;
}