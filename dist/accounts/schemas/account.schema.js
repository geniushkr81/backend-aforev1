"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountSchema = void 0;
const mongoose = require("mongoose");
exports.AccountSchema = new mongoose.Schema({
    code: String,
    name: String,
    curp: String,
    nss: String,
    email: String,
    afore: String,
    semanas: Number,
    monto: Number,
    estatus: Number
}, { timestamps: true });
//# sourceMappingURL=account.schema.js.map