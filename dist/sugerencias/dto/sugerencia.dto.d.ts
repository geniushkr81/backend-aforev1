export declare class SugerenciaDTO {
    readonly name: string;
    readonly quality: string;
    readonly product: string;
    readonly price: string;
    readonly createdAt: Date;
}
