"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SugerenciaSchema = void 0;
const mongoose = require("mongoose");
exports.SugerenciaSchema = new mongoose.Schema({
    name: String,
    quality: String,
    product: String,
    price: String,
    createdAt: Date,
}, {
    timestamps: true
});
//# sourceMappingURL=sugerencia.schema.js.map