import { SugerenciasService } from './sugerencias.service';
import { SugerenciaDTO } from './dto/sugerencia.dto';
export declare class SugerenciasController {
    private sugerencia;
    constructor(sugerencia: SugerenciasService);
    addCategory(res: any, SugerenciaDTO: SugerenciaDTO): Promise<any>;
    GetAll(res: any): Promise<any>;
}
