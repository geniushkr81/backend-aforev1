import { Sugerencia } from './interfaces/sugerencia.interface';
import { SugerenciaDTO } from './dto/sugerencia.dto';
import { Model } from 'mongoose';
export declare class SugerenciasService {
    private readonly SugerenciaModel;
    constructor(SugerenciaModel: Model<Sugerencia>);
    create(SugerenciaDTO: SugerenciaDTO): Promise<any>;
    getAll(): Promise<any>;
}
