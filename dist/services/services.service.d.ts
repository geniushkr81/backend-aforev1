import { Model } from 'mongoose';
import { Captura } from './interfaces/services.interface';
import { CreateCapturaDto } from './dto/services.dto';
export declare class ServicesService {
    private readonly capturaModel;
    constructor(capturaModel: Model<Captura>);
    getCapturas(): Promise<Captura[]>;
    getCaptura(rfc: string): Promise<Captura>;
    createCaptura(createCapturaDTO: CreateCapturaDto): Promise<Captura>;
    deleteCaptura(capturaID: string): Promise<Captura>;
    updateCaptura(capturaID: string, createCapturaDTO: CreateCapturaDto): Promise<Captura>;
}
