import { Document } from "mongoose";
export interface Captura extends Document {
    readonly name: string;
    readonly mail: string;
    readonly curp: string;
    readonly nss: string;
    readonly phone: string;
    readonly rfc: string;
    readonly status: string;
}
