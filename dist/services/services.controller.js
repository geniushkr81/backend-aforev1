"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServicesController = void 0;
const common_1 = require("@nestjs/common");
const services_dto_1 = require("./dto/services.dto");
const services_service_1 = require("./services.service");
let ServicesController = class ServicesController {
    constructor(serviceService) {
        this.serviceService = serviceService;
    }
    async createCaptura(res, createCapturaDTO) {
        const captura = await this.serviceService.createCaptura(createCapturaDTO);
        return res.status(common_1.HttpStatus.OK).json({
            message: "Captura creada",
            user: captura
        });
    }
    async getCapturas(res) {
        const capturas = await this.serviceService.getCapturas();
        return res.status(common_1.HttpStatus.OK).json({
            capturas
        });
    }
    async getCaptura(res, rfc) {
        const captura = await this.serviceService.getCaptura(rfc);
        if (!captura)
            throw new common_1.NotFoundException('Captura de AFORE no existe');
        return res.status(common_1.HttpStatus.OK).json(captura);
    }
    async deleteCaptura(res, capturaID) {
        const deletedCaptura = await this.serviceService.deleteCaptura(capturaID);
        if (!deletedCaptura)
            throw new common_1.NotFoundException('User does not exists, we cant complete the action');
        return res.status(common_1.HttpStatus.OK).json({
            message: 'User deleted',
            deletedCaptura
        });
    }
    async updateCaptura(res, createCapturaDTO, capturaID) {
        const updtaedCaptura = await this.serviceService.updateCaptura(capturaID, createCapturaDTO);
        if (!updtaedCaptura)
            throw new common_1.NotFoundException('Cpatura does not exists, we cant complete the action');
        return res.status(common_1.HttpStatus.OK).json({
            message: 'Captura updated',
            updtaedCaptura
        });
    }
};
__decorate([
    common_1.Post('/create'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, services_dto_1.CreateCapturaDto]),
    __metadata("design:returntype", Promise)
], ServicesController.prototype, "createCaptura", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ServicesController.prototype, "getCapturas", null);
__decorate([
    common_1.Get('/:rfc'),
    __param(0, common_1.Res()), __param(1, common_1.Param('rfc')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], ServicesController.prototype, "getCaptura", null);
__decorate([
    common_1.Delete('/delete'),
    __param(0, common_1.Res()), __param(1, common_1.Query('capturaID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ServicesController.prototype, "deleteCaptura", null);
__decorate([
    common_1.Put('/update'),
    __param(0, common_1.Res()), __param(1, common_1.Body()), __param(2, common_1.Query('capturaID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, services_dto_1.CreateCapturaDto, Object]),
    __metadata("design:returntype", Promise)
], ServicesController.prototype, "updateCaptura", null);
ServicesController = __decorate([
    common_1.Controller('services'),
    __metadata("design:paramtypes", [services_service_1.ServicesService])
], ServicesController);
exports.ServicesController = ServicesController;
//# sourceMappingURL=services.controller.js.map