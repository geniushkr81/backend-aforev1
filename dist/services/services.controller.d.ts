import { CreateCapturaDto } from './dto/services.dto';
import { ServicesService } from './services.service';
export declare class ServicesController {
    private serviceService;
    constructor(serviceService: ServicesService);
    createCaptura(res: any, createCapturaDTO: CreateCapturaDto): Promise<any>;
    getCapturas(res: any): Promise<any>;
    getCaptura(res: any, rfc: string): Promise<any>;
    deleteCaptura(res: any, capturaID: any): Promise<any>;
    updateCaptura(res: any, createCapturaDTO: CreateCapturaDto, capturaID: any): Promise<any>;
}
