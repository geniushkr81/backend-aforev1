"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CapturaSchema = void 0;
const mongoose_1 = require("mongoose");
exports.CapturaSchema = new mongoose_1.Schema({
    name: String,
    mail: String,
    curp: String,
    nss: String,
    phone: String,
    rfc: String,
    stauts: String
});
//# sourceMappingURL=services.schema.js.map