export declare class CreateCapturaDto {
    readonly name: string;
    readonly mail: string;
    readonly curp: string;
    readonly nss: string;
    readonly phone: string;
    readonly rfc: string;
    readonly status: string;
}
