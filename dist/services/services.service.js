"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServicesService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let ServicesService = class ServicesService {
    constructor(capturaModel) {
        this.capturaModel = capturaModel;
    }
    async getCapturas() {
        const capturas = await this.capturaModel.find();
        return capturas;
    }
    async getCaptura(rfc) {
        const captura = await this.capturaModel.findOne({ rfc: rfc });
        return captura;
    }
    async createCaptura(createCapturaDTO) {
        const captura = new this.capturaModel(createCapturaDTO);
        return await captura.save();
    }
    async deleteCaptura(capturaID) {
        const deletedCaptura = await this.capturaModel.findByIdAndDelete(capturaID);
        return deletedCaptura;
    }
    async updateCaptura(capturaID, createCapturaDTO) {
        const updatedCaptura = await this.capturaModel.findByIdAndUpdate(capturaID, createCapturaDTO, { new: true });
        return updatedCaptura;
    }
};
ServicesService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Captura')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], ServicesService);
exports.ServicesService = ServicesService;
//# sourceMappingURL=services.service.js.map