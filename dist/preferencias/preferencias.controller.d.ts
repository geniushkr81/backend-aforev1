import { PreferenciasService } from './preferencias.service';
import { PreferenciaDTO } from './dto/preferencia.dto';
export declare class PreferenciasController {
    private preferencia;
    constructor(preferencia: PreferenciasService);
    addCategory(res: any, PreferenciaDTO: PreferenciaDTO): Promise<any>;
    GetAll(res: any): Promise<any>;
}
