"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PreferenciaSchema = void 0;
const mongoose = require("mongoose");
exports.PreferenciaSchema = new mongoose.Schema({
    preferencia: String,
    createdAt: Date,
}, { timestamps: true });
//# sourceMappingURL=preferencia.schema.js.map