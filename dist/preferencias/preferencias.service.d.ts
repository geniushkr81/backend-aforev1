import { Model } from 'mongoose';
import { Preferencia } from './interfaces/preferencia.interface';
import { PreferenciaDTO } from './dto/preferencia.dto';
export declare class PreferenciasService {
    private readonly PreferenciaModel;
    constructor(PreferenciaModel: Model<Preferencia>);
    create(PreferenciaDTO: PreferenciaDTO): Promise<any>;
    getAll(): Promise<any>;
}
