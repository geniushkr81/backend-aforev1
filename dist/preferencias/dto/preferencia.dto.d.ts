export declare class PreferenciaDTO {
    readonly preferencia: string;
    readonly createdAt: Date;
}
