"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditoService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let CreditoService = class CreditoService {
    constructor(creditoModel) {
        this.creditoModel = creditoModel;
    }
    async getCreditos() {
        const creditos = await this.creditoModel.find();
        return creditos;
    }
    async getCredito(rfc) {
        const credito = await this.creditoModel.findOne({ rfc: rfc });
        return credito;
    }
    async createCredito(createCreditoDto) {
        const credito = new this.creditoModel(createCreditoDto);
        return await credito.save();
    }
    async updateCredito(creditoID, createCreditoDto) {
        const updatedCredito = await this.creditoModel.findByIdAndUpdate(creditoID, createCreditoDto, { new: true });
        return updatedCredito;
    }
    async deleteCredito(creditoID) {
        const deletedCredito = await this.creditoModel.findByIdAndDelete(creditoID);
        return deletedCredito;
    }
};
CreditoService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Credito')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], CreditoService);
exports.CreditoService = CreditoService;
//# sourceMappingURL=credito.service.js.map