import { Model } from 'mongoose';
import { Credito } from './interfaces/credito.interface';
import { CreateCreditoDto } from './dto/credito.dto';
export declare class CreditoService {
    private readonly creditoModel;
    constructor(creditoModel: Model<Credito>);
    getCreditos(): Promise<Credito[]>;
    getCredito(rfc: string): Promise<Credito>;
    createCredito(createCreditoDto: CreateCreditoDto): Promise<Credito>;
    updateCredito(creditoID: string, createCreditoDto: CreateCreditoDto): Promise<Credito>;
    deleteCredito(creditoID: string): Promise<Credito>;
}
