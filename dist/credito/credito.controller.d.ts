import { CreateCreditoDto } from './dto/credito.dto';
import { CreditoService } from './credito.service';
export declare class CreditoController {
    private creditoService;
    constructor(creditoService: CreditoService);
    createCredito(res: any, createCreditoDto: CreateCreditoDto): Promise<any>;
    getCreditos(res: any): Promise<any>;
    getCredito(res: any, rfc: string): Promise<any>;
    updateCredito(res: any, createCreditoDto: CreateCreditoDto, creditoId: any): Promise<any>;
    deleteCredito(res: any, creditoID: any): Promise<any>;
}
