"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditoController = void 0;
const common_1 = require("@nestjs/common");
const credito_dto_1 = require("./dto/credito.dto");
const credito_service_1 = require("./credito.service");
let CreditoController = class CreditoController {
    constructor(creditoService) {
        this.creditoService = creditoService;
    }
    async createCredito(res, createCreditoDto) {
        const credito = await this.creditoService.createCredito(createCreditoDto);
        return res.status(common_1.HttpStatus.OK).json({
            message: 'Credito ha sido creado',
            credit: credito
        });
    }
    async getCreditos(res) {
        const creditos = await this.creditoService.getCreditos();
        return res.status(common_1.HttpStatus.OK).json({
            creditos
        });
    }
    async getCredito(res, rfc) {
        const credito = await this.creditoService.getCredito(rfc);
        if (!credito)
            throw new common_1.NotFoundException('Credito no existe');
        return res.status(common_1.HttpStatus.OK).json(credito);
    }
    async updateCredito(res, createCreditoDto, creditoId) {
        const updatedCredito = await this.creditoService.updateCredito(creditoId, createCreditoDto);
        if (!updatedCredito)
            throw new common_1.NotFoundException('El Credito no existe, no podemos completar esta accion');
        return res.status(common_1.HttpStatus.OK).json({
            message: 'Credito actualizado',
            updatedCredito
        });
    }
    async deleteCredito(res, creditoID) {
        const deletedCredito = await this.creditoService.deleteCredito(creditoID);
        if (!deletedCredito)
            throw new common_1.NotFoundException('El Credito no existe, no podemos completar esta acción');
        return res.status(common_1.HttpStatus.OK).json({
            message: 'Credito eliminado',
            deletedCredito
        });
    }
};
__decorate([
    common_1.Post('/create'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, credito_dto_1.CreateCreditoDto]),
    __metadata("design:returntype", Promise)
], CreditoController.prototype, "createCredito", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CreditoController.prototype, "getCreditos", null);
__decorate([
    common_1.Get('/:rfc'),
    __param(0, common_1.Res()), __param(1, common_1.Param('rfc')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], CreditoController.prototype, "getCredito", null);
__decorate([
    common_1.Put('/update'),
    __param(0, common_1.Res()), __param(1, common_1.Body()), __param(2, common_1.Query('creditoID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, credito_dto_1.CreateCreditoDto, Object]),
    __metadata("design:returntype", Promise)
], CreditoController.prototype, "updateCredito", null);
__decorate([
    common_1.Delete('/delete'),
    __param(0, common_1.Res()), __param(1, common_1.Query('creditoID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CreditoController.prototype, "deleteCredito", null);
CreditoController = __decorate([
    common_1.Controller('credito'),
    __metadata("design:paramtypes", [credito_service_1.CreditoService])
], CreditoController);
exports.CreditoController = CreditoController;
//# sourceMappingURL=credito.controller.js.map