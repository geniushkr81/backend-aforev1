"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditoSchema = void 0;
const mongoose_1 = require("mongoose");
exports.CreditoSchema = new mongoose_1.Schema({
    name: String,
    rfc: String,
    mail: String,
    phone: String,
    age: String,
    monto: String,
    tienecred: String,
    status: String
});
//# sourceMappingURL=credito.schema.js.map