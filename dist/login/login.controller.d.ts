import { LoginService } from './login.service';
import { LoginDTO } from './dto/login.dto';
export declare class LoginController {
    private member;
    constructor(member: LoginService);
    addLogin(res: any, LoginDTO: LoginDTO): Promise<any>;
    GetMember(Res: any, info: string): Promise<any>;
    getUsuarios(res: any): Promise<any>;
    GetAll(res: any): Promise<any>;
}
