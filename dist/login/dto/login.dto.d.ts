export declare class LoginDTO {
    readonly email: string;
    readonly passw: string;
    readonly name: string;
    readonly phone: string;
}
